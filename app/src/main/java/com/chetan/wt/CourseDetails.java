package com.chetan.wt;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CourseDetails extends AppCompatActivity {
    TextView name, agenda, date, time, duration, venue, tname;
    Button delete;
    String cid;
    Intent in;
    String sid;

    DatabaseReference reff;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_details);

        name = (TextView)findViewById(R.id.Cname);
        tname = (TextView)findViewById(R.id.Tname);
        agenda = (TextView)findViewById(R.id.Cagenda);
        date = (TextView)findViewById(R.id.Cdate);
        time = (TextView)findViewById(R.id.Ctime);
        duration = (TextView)findViewById(R.id.Cduration);
        venue = (TextView)findViewById(R.id.Cvenue);
        cid = getIntent().getStringExtra("CourseID");
        //Toast.makeText(this,ID,Toast.LENGTH_SHORT).show();

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        sid = user.getUid();

        reff = FirebaseDatabase.getInstance().getReference("Student Courses").child(sid).child(cid);
        reff.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.getChildrenCount()!=0) {
                    name.setText(dataSnapshot.child("name").getValue().toString());
                    agenda.setText(dataSnapshot.child("agenda").getValue().toString());
                    date.setText(dataSnapshot.child("date").getValue().toString());
                    tname.setText(dataSnapshot.child("tname").getValue().toString());
                    time.setText(dataSnapshot.child("start").getValue().toString());
                    duration.setText(dataSnapshot.child("duration").getValue().toString());
                    venue.setText(dataSnapshot.child("venue").getValue().toString());
                }
                else
                    finish();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        delete = (Button)findViewById(R.id.delete);
        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(CourseDetails.this);

                builder.setMessage("Are you sure you want to Delete this Course?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DatabaseReference del = FirebaseDatabase.getInstance().getReference("Student Courses").child(sid).child(cid);
                        del.setValue(null);
                        Toast.makeText(getApplicationContext(),"Deleted Succesfully",Toast.LENGTH_SHORT).show();
                        Intent in =new Intent(getApplicationContext(),CourseList.class);
                        startActivity(in);
                    }
                }).setNegativeButton("Cancel",null);
                AlertDialog alert = builder.create();
                alert.show();
            }
        });


        Button back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
