package com.chetan.wt;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ValueEventListener;
public class EditProfile extends AppCompatActivity {
    EditText nameed,cityed,qfed,emailed;
    String names,citys,qfs,emails;
    String validEmail="[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z.]+";
    String validName="[a-zA-Z ]+";
    //String validPass="^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[@#$%^&+=*!%(),.':;<>/?{}|+-_])(?=\\S+$).{6,}$";
    Button b;
    private FirebaseAuth fa;
    private DatabaseReference dbr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        nameed=(EditText)findViewById(R.id.name);
        cityed=(EditText)findViewById(R.id.city);
        emailed=(EditText)findViewById(R.id.email);
        qfed=(EditText)findViewById(R.id.qualification);
        b=(Button)findViewById(R.id.save);
        fa=FirebaseAuth.getInstance();
        FirebaseUser cuser = fa.getCurrentUser();
        dbr= FirebaseDatabase.getInstance().getReference("users");
        String id=cuser.getUid();
        Intent int3=getIntent();
        dbr.child(id).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                nameed.setText(dataSnapshot.getValue(user.class).getName());
                emailed.setText(dataSnapshot.getValue(user.class).getEmail());
                qfed.setText(dataSnapshot.getValue(user.class).getDegree());
                cityed.setText(dataSnapshot.getValue(user.class).getCity());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        /*nameed.setText((String)int3.getSerializableExtra("Username"));
        cityed.setText((String)int3.getSerializableExtra("city"));
        qfed.setText((String)int3.getSerializableExtra("qf"));
        emailed.setText((String)int3.getSerializableExtra("em"));*/
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                names=nameed.getText().toString().trim();
                emails=emailed.getText().toString().trim();
                //pass=password.getText().toString();
                //cpass=cpassword.getText().toString();
                citys=cityed.getText().toString().trim();
                qfs=qfed.getText().toString().trim();
                if(names.equalsIgnoreCase(""))
                {
                    nameed.setError("This is a required field");
                }
                if(emails.equalsIgnoreCase(""))
                {
                    emailed.setError("This is a required field");
                }
                if(qfs.equalsIgnoreCase(""))
                {
                    qfed.setError("This is a required field");
                }
                if(citys.equalsIgnoreCase(""))
                {
                    cityed.setError("This is a required field");
                }
                if(names.matches(validName)&&emails.matches(validEmail)&&names.length()!=0&&emails.length()!=0&&qfs.length()!=0&&citys.length()!=0)
                {

                    FirebaseUser cuser = fa.getCurrentUser();
                    String id = cuser.getUid();
                    user us=new user(id,names,emails,qfs,citys);
                    dbr.child(id).setValue(us);
                    Intent intobj=new Intent(EditProfile.this,Profile.class);
                    startActivity(intobj);
                }
                else{
                    /*if(!pass.equals(cpass)){
                            cpassword.setError("");
                            Toast.makeText(getApplicationContext(),"Re-entered password does not match",Toast.LENGTH_LONG).show();}
                    */if(names.length()==0||emails.length()==0||qfs.length()==0||citys.length()==0){
                        Toast.makeText(getApplicationContext(),"Please fill all the fields",Toast.LENGTH_LONG).show();}
                    else if(!emails.matches(validEmail)){
                        emailed.setError("");
                        Toast.makeText(getApplicationContext(),"Invalid Email Address",Toast.LENGTH_LONG).show();}
                    else if(!names.matches(validName)){
                        nameed.setError("");
                        Toast.makeText(getApplicationContext(),"Name should not contain digits/special characters)",Toast.LENGTH_LONG).show();}
                    /*else if(!pass.matches(validPass)){
                        password.setError("");
                        Toast.makeText(getApplicationContext(),"Password should be atleast 6 characters long containing special characters,digits and alphabets without spaces",Toast.LENGTH_LONG).show();
                    }*/
                }
            }
        });
    }
}

