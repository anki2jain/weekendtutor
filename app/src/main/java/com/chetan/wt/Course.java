package com.chetan.wt;

public class Course {


    private String Name,Tname, Agenda, Date, Start, Venue, Duration,TId;


    public Course() {

    }

    public Course(String name, String agenda, String date, String start, String venue, String duration,String tname,String TId) {
        Name = name;
        Tname= tname;
        Agenda = agenda;
        Date = date;
        Start = start;
        Venue = venue;
        Duration = duration;
        this.TId = TId;
    }

    public String getTname() {
        return Tname;
    }

    public void setTname(String tname) {
        Tname = tname;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getAgenda() {
        return Agenda;
    }

    public void setAgenda(String agenda) {
        Agenda = agenda;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getStart() {
        return Start;
    }

    public void setStart(String start) {
        Start = start;
    }

    public String getTId() {
        return TId;
    }

    public void setTId(String TId) {
        this.TId = TId;
    }

    public String getVenue() {
        return Venue;
    }

    public void setVenue(String venue) {
        Venue = venue;
    }

}
