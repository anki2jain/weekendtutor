package com.chetan.wt;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class SelectCourse extends AppCompatActivity {

    DatabaseReference db, reg;
    String sid,cid;
    Course course1 = new Course();
    Intent in;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selectcourse);

        final TextView tutor = (TextView)findViewById(R.id.TNAME);
        final TextView course = (TextView)findViewById(R.id.CNAME);
        final TextView agenda = (TextView)findViewById(R.id.AGENDA);
        final TextView date = (TextView)findViewById(R.id.DATE);
        final TextView duration = (TextView)findViewById(R.id.DURATION);
        final TextView venue = (TextView)findViewById(R.id.VENUE);
        final TextView start = (TextView)findViewById(R.id.START);
        cid = getIntent().getStringExtra("CourseID");

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        sid = user.getUid();

        db = FirebaseDatabase.getInstance().getReference("Tutor Courses").child(cid);
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tutor.setText(dataSnapshot.child("tname").getValue().toString());
                course.setText(dataSnapshot.child("name").getValue().toString());
                venue.setText(dataSnapshot.child("venue").getValue().toString());
                duration.setText(dataSnapshot.child("duration").getValue().toString());
                agenda.setText(dataSnapshot.child("agenda").getValue().toString());
                date.setText(dataSnapshot.child("date").getValue().toString());
                start.setText(dataSnapshot.child("start").getValue().toString());
                course1.setTname(dataSnapshot.child("tname").getValue().toString());
                course1.setName(dataSnapshot.child("name").getValue().toString());
                course1.setVenue(dataSnapshot.child("venue").getValue().toString());
                course1.setDuration(dataSnapshot.child("duration").getValue().toString());
                course1.setAgenda(dataSnapshot.child("agenda").getValue().toString());
                course1.setDate(dataSnapshot.child("date").getValue().toString());
                course1.setStart(dataSnapshot.child("start").getValue().toString());
                course1.setTId(dataSnapshot.child("tid").getValue().toString());
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        ImageView imageView = (ImageView)findViewById(R.id.imageView2);
        imageView.setImageResource(R.drawable.tutor2);

        //FirebaseAuth SID = FirebaseAuth.getInstance();
        //sid = SID.getCurrentUser().getUid();


        Button button = (Button)findViewById(R.id.button5);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder builder = new AlertDialog.Builder(SelectCourse.this);

                builder.setMessage("Are you sure you want to Register?").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        reg = FirebaseDatabase.getInstance().getReference("Student Courses").child(sid).child(cid);
                        reg.setValue(course1);
                        Toast.makeText(getApplicationContext(), "Registered Succesfully", Toast.LENGTH_SHORT).show();
                        in = new Intent(getApplicationContext(),StudentCourses.class);
                        startActivity(in);
                    }
                }).setNegativeButton("Cancel",null);
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
    }
}
